package projet;

/**
 * Classe permettant de chronom�trer l'ex�cution du
 * programme.
 */
public class Chrono {
    private long mTemps;
    
    public Chrono() {
        
    }
    
    public void start() {
        mTemps = System.currentTimeMillis();
    }
    
    public long get() {
        return System.currentTimeMillis() - mTemps;
    }
}
