package projet;

import javax.swing.JFrame;

public class Generation {
       
    public static void genererRectAleatTEST1(int nb, IAlgorithme algo ,int BORNE_DROITE ,int BORNE_BAS) {
        for (int i = 0; i < nb; i++) {
            int x = (int)(Math.random()*BORNE_DROITE);
            int y = (int)(Math.random()*BORNE_BAS);
            int w = (int)(Math.random()*BORNE_DROITE);
            int h = (int)(Math.random()*BORNE_BAS);
            
            algo.ajoutRectangle(x, y, x+w, y+h);  
        }       
    }
    
    
    public static void genererRectAleatTEST2(int nb, IAlgorithme algo ,int BORNE_DROITE ,int BORNE_BAS) {
        for (int i = 0; i < nb; i++) {
            int x = (int)(Math.random()*BORNE_DROITE);
            int y = (int)(Math.random()*BORNE_BAS);                        
            algo.ajoutRectangle(x, y, x+1, y+1);
        }
    }
    
    
    
    public static void genererRectAleatTEST3(int nb, IAlgorithme algo ,int BORNE_DROITE ,int BORNE_BAS) {
        for (int i = 0; i < nb; i++) {
            int x = (int)(Math.random()*BORNE_DROITE);
            int y = (int)(Math.random()*BORNE_BAS);
            int w = (int)(1+(Math.sqrt(Math.random()*BORNE_DROITE)));
            int h = (int)(1+(Math.sqrt(Math.random()*BORNE_BAS)));
            
            algo.ajoutRectangle(x, y, x+w, y+h);
        }
    }
    
    public static void genererRectAleatTEST1(int nb, IAlgorithme algo ,int BORNE_DROITE ,int BORNE_BAS,int fl,int hl) {
        for (int i = 0; i < nb; i++) {
            int x = (int)(Math.random()*BORNE_DROITE);
            int y = (int)(Math.random()*BORNE_BAS);
            int w = (int)(Math.random()*BORNE_DROITE);
            int h = (int)(Math.random()*BORNE_BAS);
            
            algo.ajoutRectangle(x, y, x+w, y+h);  
        }
        JFrame jf=new FrameVisu(800,600,algo.getMrect());
    }
    
    
    public static void genererRectAleatTEST2(int nb, IAlgorithme algo ,int BORNE_DROITE ,int BORNE_BAS,int fl,int hl) {
        for (int i = 0; i < nb; i++) {
            int x = (int)(Math.random()*BORNE_DROITE);
            int y = (int)(Math.random()*BORNE_BAS);                        
            algo.ajoutRectangle(x, y, x+1, y+1);
        }
        JFrame jf=new FrameVisu(800,600,algo.getMrect());
    }
    
    
    
    public static void genererRectAleatTEST3(int nb, IAlgorithme algo ,int BORNE_DROITE ,int BORNE_BAS,int fl,int hl) {
        for (int i = 0; i < nb; i++) {
            int x = (int)(Math.random()*BORNE_DROITE);
            int y = (int)(Math.random()*BORNE_BAS);
            int w = (int)(1+(Math.sqrt(Math.random()*BORNE_DROITE)));
            int h = (int)(1+(Math.sqrt(Math.random()*BORNE_BAS)));
            
            algo.ajoutRectangle(x, y, x+w, y+h);
        }
        JFrame jf=new FrameVisu(800,600,algo.getMrect());
    }
    
}
