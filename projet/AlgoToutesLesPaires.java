package projet;

import java.util.ArrayList;
import java.util.List;

/**
 * ImplemŽntation de l'algo "ToutesLesPaires"
 * qui teste deux-a-deux chaque rectangle pour voir
 * si ils intersectent.
 */
public class AlgoToutesLesPaires implements IAlgorithme {
    public List<Rectangle> mRects;
    public int mNbIntersections;
    
    public AlgoToutesLesPaires() {
        mRects = new ArrayList<Rectangle>();
        mNbIntersections = 0;
    }
    
    @Override
    public void ajoutRectangle(int x1, int y1, int x2, int y2) {
        // On ajoute un rectangle a la liste
        Rectangle r = new Rectangle(new Point(x1,y1), new Point(x2,y2));
        mRects.add(r);
    }

    @Override
    public void prepare() {
        // Rien a preparer pour cet algo.
    }

    @Override
    public void calcule() {
        // On compare chaque rectangle deux ? deux
        final int size = mRects.size();
        Rectangle r1=null;
        Rectangle r2=null;
        // Pour chaque rectangle
        for (int i = 0; i < size; i++) {
            r1 = mRects.get(i);            
            // On compare avec chaque autre rectangle
            for (int j = i+1; j < size; j++) {   
                
                r2 = mRects.get(j);
                
                Point coinBasGaucheRectI=r1.p1;
                Point coinHautDroitRectI=r1.p2;                   
                
                Point coinBasGaucheRectII=r2.p1;
                Point coinHautDroitRectII=r2.p2;
                
                //SI LE COIN BAS GAUCHE DE R2 EST DANS LA PLAGE X de R1
                if((coinBasGaucheRectI.x <= coinBasGaucheRectII.x)  &&  (coinBasGaucheRectII.x <= coinHautDroitRectI.x)){
                    if ((r1.p1.y <= r2.p1.y && r1.p2.y >= r2.p1.y)||(r1.p1.y <= r2.p2.y && r1.p2.y >= r2.p2.y)||(r1.p1.y > r2.p1.y && r1.p2.y < r2.p2.y)){      
                        //Alors Raise le nb intersect
                        mNbIntersections++;
                    } 
                }
                
                //SI LE COIN HAUT DROIT DE R2 EST DANS LA PLAGE X de R1
                if((coinBasGaucheRectI.x <= coinHautDroitRectII.x)  &&  (coinHautDroitRectII.x <= coinHautDroitRectI.x)){
                    if ((r1.p1.y <= r2.p1.y && r1.p2.y >= r2.p1.y)||(r1.p1.y <= r2.p2.y && r1.p2.y >= r2.p2.y)||(r1.p1.y > r2.p1.y && r1.p2.y < r2.p2.y)){      
                        //Alors Raise le nb intersect
                        mNbIntersections++;
                    } 
                }                 
            }
        }
    }

    @Override
    public int getNbIntersections() {
        return mNbIntersections;
    }

    @Override
    public List getMrect() {
        return mRects;
    }

}
