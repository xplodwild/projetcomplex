package projet;

/**
 * Simple classe qui repr�sente un rectangle en fonction
 * de deux points
 */
public class Rectangle implements Comparable<Rectangle> {
    public Point p1;
    public Point p2;
    
    /**
     * Constructeur par d�faut
     * @param _1 Point bas gauche
     * @param _2 Point haut droite
     */
    public Rectangle(Point _1, Point _2) {
        p1 = _1;
        p2 = _2;
    }

    @Override
    /**
     * M�thode utilis�e par l'algo Balayge lors du tri.
     * On renvoie 0 pour les rectangles � la meme abscisse (point bas gauche),
     * -1 si le rectangle en cours est � gauche de l'argument,
     *  1 si le rectangle en cours est � droite de l'argument
     */
    public int compareTo(Rectangle r) {
        if (r.p1.x == this.p1.x)
            return 0;
        else if (this.p1.x < r.p1.x)
            return -1;
        else
            return 1;
    }
}
