package projet;

public class Programme {
       
    public static void genererRectAleat(int nb, IAlgorithme algo ,int BORNE_DROITE ,int BORNE_BAS) {
        for (int i = 0; i < nb; i++) {
            int x = (int)(Math.random()*BORNE_DROITE);
            int y = (int)(Math.random()*BORNE_BAS);
            int w = (int)(Math.random()*BORNE_DROITE);
            int h = (int)(Math.random()*BORNE_BAS);
            
            algo.ajoutRectangle(x, y, x+w, y+h);
        }
    }
}
