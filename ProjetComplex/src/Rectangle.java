

/**
 * Simple classe qui reprŽsente un rectangle en fonction
 * de deux points
 */
public class Rectangle implements Comparable<Rectangle> {
    public Point p1;
    public Point p2;
    
    /**
     * Constructeur par dŽfaut
     * @param _1 Point bas gauche
     * @param _2 Point haut droite
     */
    public Rectangle(Point _1, Point _2) {
        p1 = _1;
        p2 = _2;
    }

    @Override
    /**
     * MŽthode utilisŽe par l'algo Balayge lors du tri.
     * On renvoie 0 pour les rectangles ˆ la meme abscisse (point bas gauche),
     * -1 si le rectangle en cours est ˆ gauche de l'argument,
     *  1 si le rectangle en cours est ˆ droite de l'argument
     */
    public int compareTo(Rectangle r) {
        if (this.p1.x > r.p1.x)
            return 1;
        else if (this.p1.x < r.p1.x)
            return -1;
        else
            return 0;
    }
}
