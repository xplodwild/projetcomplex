
/**
 * Simple classe qui reprŽsente un point en fonction de
 * ses coordonnŽes x et y
 */
public class Point {
    public int x;
    public int y;
    
    public Point(int _x, int _y) {
        this.x = _x;
        this.y = _y;
    }
}
