

/**
 * Classe permettant de chronomŽtrer l'exŽcution du
 * programme.
 */
public class Chrono {
    private long mTemps;
    
    public Chrono() {
        
    }
    
    public void start() {
        mTemps = System.currentTimeMillis();
    }
    
    public long get() {
        return System.currentTimeMillis() - mTemps;
    }
}
