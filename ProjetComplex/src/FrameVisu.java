/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


    import javax.swing.*;
    import java.awt.*;
    import java.util.List;
   
   public class FrameVisu extends JFrame
   {
      private int x, y;   
      private int w = 0;
      private int h = 0;
      public List<Rectangle> mRects;
    
      public FrameVisu(int l,int h,List mRects)
      {
         super("Visualisation des rectangles"); // titre
         this.mRects=mRects;
         setSize(l, h); // dimensions frame
         setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
         setVisible(true);
      }
          
   
      public void paint(Graphics g)
      {
            
         w = getSize().width;   
         h = getSize().height;  
      
            super.paint(g);
      
         x = 15;
         y = 15;  // premiere ligne de la zone de dessin
      
                  
         for(int i=0;i<mRects.size();i++){
             if(i/1==1){
                g.setColor(Color.BLUE);
             }else{
                 if(i/2==1){
                    g.setColor(Color.BLACK);  
                 }else{
                    if(i/3==1){
                        g.setColor(Color.CYAN);  
                    }else{
                        if(i/4==1){
                            g.setColor(Color.GRAY);  
                        }else{
                            if(i/5==1){
                                g.setColor(Color.GREEN);  
                            }else{
                                 if(i/6==1){
                                    g.setColor(Color.MAGENTA);  
                                }else{
                                    if(i/7==1){
                                        g.setColor(Color.ORANGE);  
                                    }else{
                                        if(i/8==1){
                                        g.setColor(Color.PINK);  
                                        }else{
                                            if(i/9==1){
                                                g.setColor(Color.RED);  
                                            }else{
                                                if(i/10==1){
                                                    g.setColor(Color.YELLOW);  
                                                }else{
                                                     if(i==0){
                                                         g.setColor(Color.WHITE);  
                                                     }
                                                } 
                                            } 
                                        }    
                                    }
                                }
                            }    
                        }    
                    } 
                 }
             }
             
             
             Rectangle tmp=mRects.get(i);
             Polygon rectangle = new Polygon();
             rectangle.addPoint(tmp.p1.x, tmp.p2.y);//HAUT GAUCHE
             rectangle.addPoint(tmp.p2.x, tmp.p2.y);//HAUT DROIT             
             rectangle.addPoint(tmp.p2.x,tmp.p1.y); //BAS DROIT
             rectangle.addPoint(tmp.p1.x,tmp.p1.y); //BAS GAUCHE  
             
             g.drawPolygon(rectangle);
             g.fillPolygon(rectangle);
         }      
      }
   }
