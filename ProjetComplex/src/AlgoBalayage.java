

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AlgoBalayage implements IAlgorithme {
    public List<Rectangle> mRects;
    public long mNbIntersections;

    public AlgoBalayage() {
        mRects = new ArrayList<>();
        mNbIntersections = 0;
    }
    
    @Override
    public void ajoutRectangle(int x1, int y1, int x2, int y2) {
        Rectangle r = new Rectangle(new Point(x1,y1), new Point(x2,y2));
        mRects.add(r);
    }

    @Override
    public void prepare() {
        // On trie l'arraylist de rectangles via le compareTo de Rectangle
        Collections.sort(mRects);
    }

    @Override
    public void calcule() {
      
        
        //On trie le tableau de rectangle en fonction du coin inferieur gauche
        
        this.prepare();
        
        //taille du tableau
        final long size = mRects.size();
        
        // Pour chaque rectangle
        for (int i = 0; i < size; i++) {
            
            //Rectangle courant R1
            Rectangle r1 = mRects.get(i);  
            
            for (int j = i+1; j < size; j++) {
			
                //Rectangle de parcours R2, dont p1.x est obligatoirement >= à r1.p1.x
                Rectangle r2 = mRects.get(j);
                
                //Si p1.x de R2 est plus petit que p2.x de R1    =>   donc les X se croisent
                if (r1.p2.x >= r2.p1.x) {                     
                    //Si p1 ou p2 de R2 croise R1    =>      croisement des Y
                    if ((r1.p1.y <= r2.p1.y && r1.p2.y >= r2.p1.y)||(r1.p1.y <= r2.p2.y && r1.p2.y >= r2.p2.y)||(r1.p1.y > r2.p1.y && r1.p2.y < r2.p2.y)){      
                        //Alors Raise le nb intersect
                        mNbIntersections++;
                    }    
                //SI x du coin inf gauche de R est plus GRAND que Sup droit de Rcourant on casse la boucle car mRects est triée en fct des x
                }else break;
            }
            
            
        }
    }

    @Override
    public long getNbIntersections() {
        return mNbIntersections;
    }

    @Override
    public List getMrect() {
        return mRects;
    }
}
