
import java.util.List;

/**
 * Interface pour les algorithmes.
 * Les diffŽrent algos implŽmentent cette interface, et on
 * l'utilise directement pour travailler de faon uniforme
 * sur les algos depuis le programme.
 */
public interface IAlgorithme {
    /**
     * Ajoute un rectangle (ses coordonnŽes) ˆ traiter par l'algorithme
     * @param x1 Abscisse du coin haut-gauche
     * @param y1 OrdonnŽe du coin haut-gauche
     * @param x2 Abscisse du coin bas-droite
     * @param y2 OrdonnŽe du coin bas-droite
     */
    
    
    
    public void ajoutRectangle(int x1, int y1, int x2, int y2);
    
    /**
     * Effectue les opŽrations nŽcessaires pour que l'algorithme
     * puisse traiter les rectangles, par exemple le tri des rectangles
     * pour l'algo de Balayage (afin de chronomŽtrer le tri et le test
     * sŽparŽment).
     */
    public void prepare();
    
    /**
     * Calcule les intersections entre les rectangles. Le nombre d'inter-
     * sections peut tre rŽcupŽrŽ par getNbIntersections()
     */
    public void calcule();
    
    /**
     * @return Le nombre d'intersections aprs calcul
     */
    public long getNbIntersections();

    public List getMrect();
}
